# Net技术栈文档

### 开发环境：


 **Windows 7/10 + VS2017 + MSSQL** 


DDD(领域驱动设计) + 命令查询的责任分离(CQRS) + [事件溯源(Event Sourcing)](https://www.cnblogs.com/netfocus/archive/2012/02/12/2347911.html)


```
敏捷开发模式：推荐使用BDD

在.net技术栈下BDD工具：SpecFlow
Github:  https://github.com/techtalk/SpecFlow
官网:  http://specflow.org/


开发模式描述：
行为驱动开发(BDD) 
测试驱动开发(TDD)
可接受性测试(TADD)


系统建模方式：
事件风暴(Event Storming)
简单描述在下面
```
领域驱动设计推荐文章：http://www.cnblogs.com/cnblogsfans/p/4284462.html




### 推荐框架No.1：ENode 


ENode是一个.NET平台下，纯C#开发的，基于DDD,CQRS,ES,EDA,In-Memory架构风格的，可以帮助开发者开发高并发、高吞吐、可伸缩、可扩展的应用程序的一个应用开发框架。


特点：国内大神开发。文档易懂，方便交流。

缺点：没有使用过。不具备评价能力。

 **其它基于DDD,CQRS,ES 开发框架：** 

EventFlow ：  **使用Async/await并基于CQRS+ES的轻量级DDD框架。** 

Github: https://github.com/eventflow/EventFlow

文档：http://docs.geteventflow.net/GettingStarted.html

### 推荐框架No.2：ABP

基于DDD和最佳实践的健壮的体系模型

特点：

拥有现成的基础功能

多租户（针对我们多仓库可能是一个不错的解决方案）

易上手

社区生态完善，文档完善。


缺点：没有CQRS，ES(Event Sourcing)


### 推荐框架No.3：微服务架构

 **1、 Service Fabric** 
微软作为.NET的主战场，自然在当前的微服务框架上有着丰富的经验，这一领域的代表作就是Azure Service Fabric，在Azure上的众多云服务都是基于Servie Fabric构建，而且微软已经明确表态了Service Fabric将开源，Runtime还没开源，说有计划开源，但目前因为依赖于很多内部工具、组件，在逐步推进runtime的开源进程.

这里列出1个Github 以Service Fabric 开发微服务的项目：
https://github.com/danielmarbach/Microservices.ServiceFabric


 **2、Steeltoe OSS** 
在微服务架构这方面Spring Cloud具有非常高的人气，如果你在使用Spring Cloud，可以使用开源项目https://github.com/steeltoeoss 开发.NET Core应用，具体可以参考这篇文章Enabling .NET Core Microservices with Steeltoe and Pivotal Cloud Foundry .

 **3、Microdot Framework** 
https://github.com/gigya/microdot，这是一个4月份才开源的.NET微服务框架，让您专注于编写定义服务逻辑的代码，无需解决开发分布式系统的无数挑战，可以很好的Microsoft Orleans的集成。

 **4、Ocelot** 
https://github.com/TomPallister/Ocelot ，Ocelot是一个使用.NET Core平台上的一个API Gateway，这个项目的目标是在.NET上面运行微服务架构

参考：http://www.cnblogs.com/shanyou/p/7440074.html




### 简单的实现CQRS：

![CQRS描述](https://gitee.com/uploads/images/2018/0124/185907_d0ca643c_667820.png "屏幕截图.png")


### 事件风暴(Event Storming) 
事件风暴在DDD中解决从领域到架构的一种方式

> 事件风暴就是把所有的关键参与者都召集到一个很宽敞的屋子里来开会，并且使用便利贴来描述系统中发生的事情。

一张桔黄色的便利贴代表一个领域事件，在上面用一句过去时的话描述曾经发生过什么事情，格式一般是：xx 已 xx。于是，我们需要整理系统相关的所有事件，也因此需要业务与开发人员共同进行风暴。如针对一个订单，会有这么一些相关的事件：



- 订单已创建
- 订单已支付
- 订单已投诉
- 订单已撤销

便会产生相关的便利贴：

![输入图片说明](https://gitee.com/uploads/images/2018/0124/192111_067df59f_667820.png "屏幕截图.png")


再按事件发生的时间轴，来对这些事件发生的顺序进行排序：

![输入图片说明](https://gitee.com/uploads/images/2018/0124/192141_6c0e845a_667820.png "屏幕截图.png")

紧接着，我们需要结合软件的用户的相关操作，写着与这些操作相关的命令。然后，结合这些命令与事件。如订单相关的命令就有：

提交订单，可以触发事件『订单已创建』

提交投诉，可以触发事件『订单已投诉』

等等

完成这个之后， 我们就有了系统相关的所有事件与命令：

![输入图片说明](https://gitee.com/uploads/images/2018/0124/192200_228496ba_667820.png "屏幕截图.png")

换句话来说，这些相关的事件与命令就是我们编写细节代码时，需要完成的功能。最后，完成相关的聚合，我们就可以得到一份完整的模型：

![输入图片说明](https://gitee.com/uploads/images/2018/0124/192214_701e9563_667820.png "屏幕截图.png")


依据这个模型，我们可以轻松地做出微服务设计。

![输入图片说明](https://gitee.com/uploads/images/2018/0124/193057_01a418c1_667820.png "屏幕截图.png")

作者：phodal
链接：https://www.zhihu.com/question/68611994/answer/300614212
来源：知乎
著作权归作者所有，转载请联系作者获得授权。。


### 行为驱动开发

介绍：https://www.jianshu.com/p/178ea886209e


其它文章：http://www.cnblogs.com/jarodzz/archive/2012/07/02/2573014.html
